package br.com.omni.treinamento.domain.entities.ws.response;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.net.URI;
import java.util.List;
import java.util.Map;

import static org.springframework.http.HttpStatus.*;

public final class ResponseEntityBuilder {

    private Object entity;
    private HttpStatus httpStatus;
    private RequestCache requestCache;
    private ResponseCache responseCache;

    private ResponseEntityBuilder() {
        this.requestCache = new RequestCache();
        this.responseCache = new ResponseCache();
    }

    public ResponseEntityBuilder addEntity(final Object entity) {
        this.entity = entity;
        return this;
    }

    public ResponseEntityBuilder addLastModified(final Long lastModified) {
        this.responseCache.lastModified = LastModified.from(lastModified);
        return this;
    }

    public ResponseEntityBuilder addETagValue(final String value) {
        this.responseCache.eTag = ETag.from(value);
        return this;
    }

    public ResponseEntityBuilder addLocation(final URI location) {
        this.responseCache.location = Location.from(location);
        return this;
    }

    public ResponseEntityBuilder addRequestHeaders(final Map<String, List<String>> headersMap) {
        this.requestCache.headers.putAll(headersMap);
        return this;
    }

    public ResponseEntityBuilder addHttpStatus(final HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
        return this;
    }

    private boolean matchLastModified() {
        LastModified fromRequest = requestCache.getIfModifiedSince();
        LastModified fromResponse = responseCache.lastModified;

        if (fromResponse.equals(LastModified.NO_LAST_MODIFIED)) {
            return false;
        }

        return fromRequest.equals(fromResponse);
    }

    private boolean matchETag() {
        ETag fromRequest = requestCache.getIfNoneMatch();
        ETag fromResponse = responseCache.eTag;

        if (fromRequest.equals(ETag.NO_ETAG)) {
            return false;
        }

        return fromRequest.equals(fromResponse);
    }

    private HttpStatus getStatusOrDefault(final HttpStatus statusDefault) {
        return this.httpStatus != null ? this.httpStatus : statusDefault;
    }

    public ResponseEntity<Object> build() {
        HttpHeaders responseHeaders = responseCache.getHeaders();
        try {
            if (entity != null) {
                if (matchLastModified() || matchETag()) {
                    return new ResponseEntity<>(responseHeaders, NOT_MODIFIED);
                }
                return new ResponseEntity<>(entity, responseHeaders, getStatusOrDefault(OK));
            }

            return new ResponseEntity<>(responseHeaders, getStatusOrDefault(NO_CONTENT));
        } finally {
            clear();
        }
    }

    private void clear() {
        entity = null;
        httpStatus = null;
        requestCache = null;
        responseCache = null;
    }

    public static ResponseEntityBuilder builder() {
        return new ResponseEntityBuilder();
    }

    private static class RequestCache {

        HttpHeaders headers = new HttpHeaders();

        ETag getIfNoneMatch() {
            return ETag.from(headers);
        }

        LastModified getIfModifiedSince() {
            return LastModified.from(headers);
        }
    }

    private static class ResponseCache {

        ETag eTag = ETag.NO_ETAG;
        LastModified lastModified = LastModified.NO_LAST_MODIFIED;
        Location location = Location.NO_LOCATION;

        HttpHeaders getHeaders() {
            HttpHeaders headers = new HttpHeaders();
            eTag.addTo(headers);
            lastModified.addTo(headers);
            location.addTo(headers);
            return headers;
        }

    }
}
