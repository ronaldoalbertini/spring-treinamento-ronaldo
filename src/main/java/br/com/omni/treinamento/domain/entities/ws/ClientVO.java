package br.com.omni.treinamento.domain.entities.ws;

import lombok.*;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ClientVO {

    private long id;
    private String firstName;
    private String lastName;
    private String email;

}