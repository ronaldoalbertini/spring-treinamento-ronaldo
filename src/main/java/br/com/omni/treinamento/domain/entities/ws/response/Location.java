package br.com.omni.treinamento.domain.entities.ws.response;

import org.springframework.http.HttpHeaders;

import java.net.URI;

public class Location {

    public static final Location NO_LOCATION = new Location(null);

    private URI value;

    private Location(final URI value) {
        this.value = value;
    }

    public static Location from(final URI value) {
        return value == null ? NO_LOCATION : new Location(value);
    }

    public HttpHeaders addTo(final HttpHeaders headers) {
        if (value == null) {
            return headers;
        }

        headers.setLocation(value);
        return headers;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        Location that = (Location) o;
        return this.value != null ? this.value.equals(that.value) : false;

    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }

}
