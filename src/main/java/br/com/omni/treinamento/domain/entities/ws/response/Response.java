package br.com.omni.treinamento.domain.entities.ws.response;

import br.com.omni.treinamento.domain.entities.ws.ErrorVO;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.http.HttpStatus;
import org.springframework.validation.FieldError;

import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class Response {

    @JsonProperty("errors")
    private Set<ErrorVO> errors;

    private String message;
    private String path;
    private String error;
    private int status;

    public class BusinessException extends RuntimeException {
        private static final long serialVersionUID = 1L;

        private final Set<ErrorVO> errors;

        public BusinessException(final String message, final Set<ErrorVO> error) {
            super(message);
            this.errors = error;
        }

        public BusinessException(String message, String code, String field) {
            super(message);
            errors = new TreeSet<>();
            errors.add(new ErrorVO(field, code));
        }

        public BusinessException(Exception e, String code, String field) {
            super(e.getMessage());
            errors = new TreeSet<>();
            errors.add(new ErrorVO(field, code));
        }

        public void addError(ErrorVO error) {
            this.errors.add(error);
        }

        public Set<ErrorVO> getErrors() {
            return this.errors;
        }
    }

    public Response(ValidationException ex, HttpStatus httpStatus, String path) {
        this.errors = ex.getErrors();
        this.message = ex.getMessage();
        this.status = httpStatus.value();
        this.error = httpStatus.getReasonPhrase();
        this.path = path;
    }

    public Response(BusinessException ex, HttpStatus httpStatus, String path) {
        this.errors = ex.getErrors();
        this.message = ex.getMessage();
        this.status = httpStatus.value();
        this.error = httpStatus.getReasonPhrase();
        this.path = path;
    }

    public Response(String message, HttpStatus httpStatus, String path) {
        this.message = message;
        this.status = httpStatus.value();
        this.error = httpStatus.getReasonPhrase();
        this.path = path;
    }

    public Response(Properties errorMap, String message, List<FieldError> fieldErrors, HttpStatus httpStatus) {
        this(message, httpStatus, null);
        errors = new TreeSet<>();
        fieldErrors.forEach(fieldError -> errors.add(new ErrorVO(errorMap, fieldError)));
    }

    public String getMessage() {
        return message;
    }

    public Set<ErrorVO> getErrors() {
        return errors;
    }

    public int getStatus() {
        return status;
    }

    public String getError() {
        return error;
    }

    public String getPath() {
        return path;
    }
}