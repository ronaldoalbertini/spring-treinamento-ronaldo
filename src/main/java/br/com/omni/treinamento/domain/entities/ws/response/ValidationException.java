package br.com.omni.treinamento.domain.entities.ws.response;
;

import br.com.omni.treinamento.domain.entities.ws.ErrorVO;

import java.util.Set;
import java.util.TreeSet;

public class ValidationException extends RuntimeException {

    private static final long serialVersionUID = 2419716588433858288L;

    private final Set<ErrorVO> errors;

    public ValidationException() {
        errors = new TreeSet<>();
    }

    public ValidationException(final Set<ErrorVO> errors) {
        this.errors = errors;
    }

    public ValidationException(final String message, final Set<ErrorVO> errors) {
        super(message);
        this.errors = errors;
    }

    public void shouldThrow() {
        if (!errors.isEmpty()) {
            throw this;
        }
    }

    public void addError(final ErrorVO error) {
        this.errors.add(error);
    }

    public Set<ErrorVO> getErrors() {
        return this.errors;
    }
}
