package br.com.omni.treinamento.domain.entities.ws.response;

import org.springframework.http.HttpHeaders;

public class LastModified {

    public static final LastModified NO_LAST_MODIFIED = new LastModified(null);

    private Long value;

    private LastModified(final Long value) {
        this.value = value;
    }

    public static LastModified from(final Long value) {
        if (value == null || value.equals(Long.valueOf(-1))) {
            return NO_LAST_MODIFIED;
        }

        return new LastModified(value);
    }

    public static LastModified from(final HttpHeaders requestHeaders) {
        return from(requestHeaders.getIfModifiedSince());
    }

    public HttpHeaders addTo(final HttpHeaders headers) {
        if (value == null) {
            return headers;
        }

        headers.setLastModified(value);
        return headers;
    }

    @Override
    public int hashCode() {
        return value != null ? (int) (value / 1000) : 0;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        LastModified that = (LastModified) o;
        return value != null && that.value != null ? (value / 1000 == that.value / 1000) : false;
    }
}