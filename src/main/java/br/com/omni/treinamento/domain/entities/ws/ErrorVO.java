package br.com.omni.treinamento.domain.entities.ws;

import com.fasterxml.jackson.annotation.JsonView;
import com.google.common.collect.ComparisonChain;
import lombok.*;
import org.apache.commons.lang3.ObjectUtils;
import org.springframework.validation.FieldError;

import javax.validation.ConstraintViolation;
import java.io.Serializable;
import java.util.Properties;

@Getter
@Setter
@EqualsAndHashCode(of = {"code", "field"})
@ToString
@NoArgsConstructor
public class ErrorVO implements Comparable<ErrorVO>, Serializable {
    private static final long serialVersionUID = 1L;

    public interface ErrorView {
    }

    @JsonView(ErrorView.class)
    private String field;

    @JsonView(ErrorView.class)
    private String message;

    @JsonView(ErrorView.class)
    private String code;

    @JsonView(ErrorView.class)
    private Object rejected;

    public ErrorVO(final String field, final String code, final String message, final Object rejected) {
        this.field = field;
        this.message = message;
        this.code = code;
        this.rejected = rejected;
    }

    public ErrorVO(final String field, final String code) {
        this.field = field;
        this.code = code;
        message = null;
        rejected = null;
    }

    public ErrorVO(final Properties errorMap, final FieldError fieldError) {
        this.field = fieldError.getField();
        this.message = fieldError.getDefaultMessage();
        this.code = getMappedCode(errorMap, fieldError.getCode());
        this.rejected = fieldError.getRejectedValue();
    }

    public <T> ErrorVO(final Properties errorMap, final ConstraintViolation<T> violation) {
        final String name =
                violation.getConstraintDescriptor().getAnnotation().annotationType().getName();
        this.code = getMappedCode(errorMap, name);
        this.field = violation.getPropertyPath().toString();
        this.message = violation.getMessage();
        this.rejected = violation.getInvalidValue();
    }

    private String getMappedCode(final Properties errorMap, final String code) {
        return errorMap != null ? ObjectUtils.firstNonNull(errorMap.get(code), code).toString() : code;
    }

    @Override
    public int compareTo(final ErrorVO other) {
        return ComparisonChain.start().compare(field, other.field).compare(code, other.code).result();
    }

}