package br.com.omni.treinamento.domain.entities.ws.response;

import com.google.common.base.Strings;
import org.springframework.http.HttpHeaders;

import static org.springframework.util.StringUtils.trimLeadingCharacter;
import static org.springframework.util.StringUtils.trimTrailingCharacter;

public class ETag {

    public static final ETag NO_ETAG = new ETag(null);

    private final String value;

    private ETag(final String value) {
        this.value = trimTrailingCharacter(trimLeadingCharacter(value, '"'), '"');
    }

    public static ETag from(final String value) {
        return Strings.isNullOrEmpty(value) ? NO_ETAG : new ETag(value);
    }

    public static ETag from(final HttpHeaders requestHeaders) {
        return requestHeaders.getIfNoneMatch().isEmpty() ? NO_ETAG : from(requestHeaders.getIfNoneMatch().get(0));
    }

    public HttpHeaders addTo(final HttpHeaders headers) {
        String stringValue = toString();
        if (Strings.isNullOrEmpty(stringValue)) {
            return headers;
        }

        headers.setETag(stringValue);
        return headers;
    }

    @Override
    public String toString() {
        return Strings.isNullOrEmpty(value) ? "" : "\"".concat(value).concat("\"");
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        ETag that = (ETag) o;
        return this.value != null ? this.value.equals(that.value) : false;

    }

    @Override
    public int hashCode() {
        return value != null ? value.hashCode() : 0;
    }
}