package br.com.omni.treinamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SpringTreinamentoRonaldoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SpringTreinamentoRonaldoApplication.class, args);
	}
}
