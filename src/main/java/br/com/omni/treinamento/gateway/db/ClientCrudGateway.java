package br.com.omni.treinamento.gateway.db;

import br.com.omni.treinamento.domain.entities.Client;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;


@Transactional
@Repository
public interface ClientCrudGateway extends PagingAndSortingRepository<Client,Long> {

    List<Client> findClientByEmail(String email);
    List<Client> findClientByLastNameOrFirstName(String lastName, String firstName);
}
