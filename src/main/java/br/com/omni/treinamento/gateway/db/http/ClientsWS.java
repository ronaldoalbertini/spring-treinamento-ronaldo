package br.com.omni.treinamento.gateway.db.http;

import br.com.omni.treinamento.domain.entities.ws.ClientVO;
import br.com.omni.treinamento.exception.ResourceNotFoundException;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.ACCEPTED;
import lombok.extern.slf4j.Slf4j;
import java.util.List;

@RestController
@Slf4j
@RequestMapping(value= URLMapping.ROOT_PATH)
public class ClientsWS {

    @ApiOperation(value="Get All Clients")
    @ApiResponses({
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "Not Found"),
    })
    @RequestMapping(method=RequestMethod.GET, path="/clients")
    public ResponseEntity<List<ClientVO>> getAll(){
        final List<ClientVO> clients = null; //action.getAllClients();

        if(clients.isEmpty()){
            throw new ResourceNotFoundException("ResourceNotFoundException");
        }
        return new ResponseEntity(ACCEPTED);
    }
}